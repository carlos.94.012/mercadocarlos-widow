using System.Collections;
using UnityEngine;

public class TournamentPrint : MonoBehaviour
{
    [SerializeField] GameObject prefabInfo;
    
    [SerializeField] GameObject listaVertical;
    [SerializeField] RectTransform lista;

    float heightInfo;

    void Start()
    {
        heightInfo = prefabInfo.GetComponent<RectTransform>().sizeDelta.y;
        lista.offsetMin = new Vector2(lista.offsetMin.x,0);
        prefabInfo.SetActive(false);
        StartCoroutine(PrintLista());
    }

    IEnumerator PrintLista()
    {
        while (!pubgTournamentGet.Instance.IsReady)
        {
            yield return null;
        }

        foreach (Tournament item in pubgTournamentGet.Instance.MyTournaments.data)
        {
            GameObject Info = Instantiate(prefabInfo, listaVertical.transform);
            Info.SetActive(true);
            Info.GetComponent<newInfo>().SetText(item.id, item.attributes.createdAt);
        }

        lista.offsetMin = new Vector2(lista.offsetMin.x, -heightInfo * pubgTournamentGet.Instance.MyTournaments.data.Length);
    }
}
