using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class newInfo : MonoBehaviour
{
    [SerializeField] TMP_Text txtID;
    [SerializeField] TMP_Text txtDate;

    public void SetText(string ID, string DATE)
    {
        txtID.text = ID;
        txtDate.text = DATE;
    }
}
