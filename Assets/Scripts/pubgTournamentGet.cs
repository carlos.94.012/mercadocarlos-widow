using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[System.Serializable]
public class Tournament
{
    public string type;
    public string id;
    public Attributes attributes;
}

[System.Serializable]
public class Attributes
{
    public string createdAt;
}

[System.Serializable]
public class Tournaments
{
    public Tournament[] data;
}

public class pubgTournamentGet : MonoBehaviour
{
    #region Singleton
    static pubgTournamentGet instance;
    public static pubgTournamentGet Instance { get { return instance; } }

    void Awake()
    {
        if (Instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion Singleton

    [SerializeField] string url;
    [SerializeField] string key = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIwMmJkYTljMC1hY2E5LTAxM2EtZGQxNi0wZDNmNWNjYWIwOWMiLCJpc3MiOiJnYW1lbG9ja2VyIiwiaWF0IjoxNjUxNTM5MDU0LCJwdWIiOiJibHVlaG9sZSIsInRpdGxlIjoicHViZyIsImFwcCI6ImNhcmxvc21lcmNhZG8ifQ.hwB9a6c9k4rKPtJ1_L-vpfP8Mi1u3WH_wDHeESbizz4";

    [SerializeField] Tournaments myTournaments = new Tournaments();
    public Tournaments MyTournaments { get => myTournaments; }

    bool isReady;
    public bool IsReady { get => isReady; }
    

    private void Start()
    {
        StartCoroutine(GetValues());
    }

    IEnumerator GetValues()
    {
        using (UnityWebRequest rq = UnityWebRequest.Get(url))
        {
            rq.SetRequestHeader("Authorization", "Bearer " + key);
            rq.SetRequestHeader("Accept", "application/vnd.api+json");

            yield return rq.SendWebRequest();


            if (rq.result == UnityWebRequest.Result.Success )
            {
                Debug.Log("Request Success ");
                myTournaments = JsonUtility.FromJson<Tournaments>(rq.downloadHandler.text);
                yield return new WaitForEndOfFrame();
                isReady = true;
            }
            else
            {
                Debug.Log("Error: " + rq.error);                
            }
        }
    }


}
